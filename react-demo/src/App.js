// import logo from './logo.svg';


// function App() {
//   return (
//     <div className="App">
//       {/* html can be run by react => jsx not vanilla js so can have embedded html */}
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Gang sheet
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }


// it uses a shadow dom doesnt need to redraw the whole document again




import './App.css';
import Fruit from './fruit';
import {FruitClass} from './fruit';

function App() {
  return (
    <div className="App">
      <Fruit />
      <FruitClass />
    </div>
  );
}

export default App;
