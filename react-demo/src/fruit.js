import React from 'react';
import './App.css';

function fruit() {
    let fruit={
        name:"apple",
        color:"red",
        quantity:10,
    }
    const fruitcolor=<p> The color of the fruit is {fruit.color}</p>
    return (
        <div className="App">
            <h1> This is a fruit</h1>
            {fruitcolor}
        </div>
    );
}

export default fruit();

class FruitClass extends React.Component{
    constructor(){
        this.state={
            fruit: {
                name:"apple",
                color:"red",
                quantity:10,
            }
        }
    }
    render(){
        const fruitcolor=
        <>
            <p> The color of the fruit is {this.state.fruit.color}</p>

        </>
        
    }
}

export {FruitClass};


