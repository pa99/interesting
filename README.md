
# Interesting project

## Description
The "interesting" jupyter lab notebook does something very simple to showcase that git, gitlab and vscode are all working as intended. 

I have created the notebook on my laptop and then I will git add to stage the file. I will then commit the file with a message and push it to gitlab. 

I then created a second jupyter notebook titled "sine" which displays a simple sine wave.


## Project status
Depending on the trainings, this project might not be continued.